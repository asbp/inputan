package uinbdg.fisip.inputnilai.ui.layoutmanager

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager

class HomeMenuLayoutManager(context: Context?, spanCount: Int) :
    GridLayoutManager(context, spanCount) {

    override fun canScrollHorizontally(): Boolean {
        return false
    }

    override fun canScrollVertically(): Boolean {
        return false
    }
}
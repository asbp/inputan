package uinbdg.fisip.inputnilai.ui.activities

import android.os.Bundle
import uinbdg.fisip.inputnilai.R
import uinbdg.fisip.inputnilai.ui.activities.base.BaseLoggedActivity

class AboutActivity : BaseLoggedActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        header("About")
    }
}

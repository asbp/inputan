package uinbdg.fisip.inputnilai.ui.activities

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import uinbdg.fisip.inputnilai.R
import uinbdg.fisip.inputnilai.data.MoreMenu
import uinbdg.fisip.inputnilai.ui.activities.base.BaseLoggedActivity
import uinbdg.fisip.inputnilai.ui.adapters.MoreMenuAdapter
import uinbdg.fisip.inputnilai.ui.adapters.SimpleSectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_more_menu.*
import uinbdg.fisip.inputnilai.ui.adapters.SimpleSectionedRecyclerViewAdapter.Section
import uinbdg.fisip.inputnilai.utils.isEven
import uinbdg.fisip.inputnilai.utils.safe

class MoreMenuActivity : BaseLoggedActivity() {

    private val menus: ArrayList<MoreMenu> = ArrayList()
    private val sections: ArrayList<Section> = ArrayList()

    private val menuAdapter: MoreMenuAdapter = MoreMenuAdapter(menus)

    private lateinit var sectionAdapter: SimpleSectionedRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_menu)

        header("Lebih Banyak")

        sectionAdapter = SimpleSectionedRecyclerViewAdapter(this, R.layout.item_menu_section, R.id.section_text, menuAdapter)

        more_menu_rv.layoutManager = LinearLayoutManager(this)
        more_menu_rv.itemAnimator = DefaultItemAnimator()

        addMenus()

        val dummy = arrayOfNulls<Section>(sections.size)

        sectionAdapter.setSections(sections.toArray(dummy))

        more_menu_rv.adapter = sectionAdapter
    }

    private fun addSection(s: String) = sections.add(Section(menuAdapter.itemCount, s))

    private fun addMenus() {
        addSection("Akun")
        menus.add(MoreMenu("Logout", "Keluar dari akun", R.drawable.ic_logout, R.color.red_500, View.OnClickListener {  }))
        addSection("Umum")
        menus.add(MoreMenu("Tentang Aplikasi Ini", "Melihat informasi tentang aplikasi ini.", R.drawable.ic_information, R.color.blue_500, View.OnClickListener {  }))

    }
}

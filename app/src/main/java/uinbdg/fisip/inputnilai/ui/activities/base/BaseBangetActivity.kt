package uinbdg.fisip.inputnilai.ui.activities.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity

open class BaseBangetActivity: AppCompatActivity() {

    inline fun <reified T: BaseLoggedActivity> Context.createIntent(bundle: Bundle) =
        Intent(this, T::class.java).putExtras(bundle)

    protected inline fun <reified T: BaseLoggedActivity> launchActivity(bundle: Bundle = Bundle()) {
        startActivity(createIntent<T>(bundle))
    }

    protected inline fun <reified T: BaseLoggedActivity> launchActivityWithResult(bundle: Bundle = Bundle(), launchCode: Int) {
        startActivityForResult(createIntent<T>(bundle), launchCode)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        actionBar?.elevation = 0f
        supportActionBar?.elevation = 0f
    }

    private fun setBackButton() {
        actionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    protected fun header(s: String) {
        setBackButton()
        title = s
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
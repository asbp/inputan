package uinbdg.fisip.inputnilai.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uinbdg.fisip.inputnilai.R
import uinbdg.fisip.inputnilai.data.MoreMenu
import kotlinx.android.synthetic.main.item_more_menu.view.*

class MoreMenuAdapter(private val items: List<MoreMenu>): RecyclerView.Adapter<MoreMenuAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemName: TextView = itemView.more_menu_text
        val itemIcon: ImageView = itemView.more_menu_icon
        val layout: RelativeLayout = itemView.more_menu_layout
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_more_menu, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.itemName.text = item.title

        Glide.with(holder.itemView.context)
            .load(item.icon)
            .into(holder.itemIcon)

        holder.itemIcon.setColorFilter(ContextCompat.getColor(holder.itemView.context, item.color), android.graphics.PorterDuff.Mode.SRC_IN)

        holder.layout.setOnClickListener(item.onClickListener)

        holder.layout.setOnLongClickListener {
            Toast.makeText(holder.itemView.context, item.desc, Toast.LENGTH_LONG).show()
            true
        }
    }


}
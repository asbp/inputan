package uinbdg.fisip.inputnilai.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uinbdg.fisip.inputnilai.data.HomeMenu
import kotlinx.android.synthetic.main.item_main_menu.view.*
import uinbdg.fisip.inputnilai.R

class MainMenuAdapter(private val items: List<HomeMenu>): RecyclerView.Adapter<MainMenuAdapter.MyViewHolder>() {
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemLabel: TextView = itemView.main_menu_caption
        val itemIcon: ImageView = itemView.main_menu_img
        val layout: LinearLayout = itemView.main_menu_layout
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_main_menu, parent, false))
    }

    override fun getItemCount(): Int {
       return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = items[position]

        holder.itemLabel.text = item.title

        Glide.with(holder.itemView.context)
            .load(item.icon)
            .into(holder.itemIcon)

        holder.itemIcon.setColorFilter(ContextCompat.getColor(holder.itemView.context, item.color), android.graphics.PorterDuff.Mode.SRC_IN)

        holder.layout.setOnClickListener(item.onClickListener)
    }
}
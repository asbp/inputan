package uinbdg.fisip.inputnilai.utils

import android.net.Uri
import org.threeten.bp.Year

fun safe(body: () -> Unit, oops: (Exception) -> Unit = {}) =
    try { body() } catch (e: Exception) { oops(e) }

fun Int.isEven(): Boolean = (this % 2 == 0)

fun String.toUri(): Uri  = Uri.parse(this)

fun Year.isLeapYear(): Boolean {
    val y = this.value

    return when{
        y % 4 == 0 -> {
            when{
                y % 100 == 0 -> y % 400 == 0
                else -> true
            }
        }
        else -> false
    }
}